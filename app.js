var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());


// //routes
app.use('/', require('./app/routes'));


app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send({ success: false, message: 'Endpoint no encontrado. :(' });
});


//run app
const port = process.env.PORT || 3000
app.listen(port, function () {
  console.log('Node app is running on:', 'http://localhost:' + port + '/api/v1', "\n");
});
