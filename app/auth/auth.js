const express = require('express');
const app = express.Router();
const crud = require('../database/crud');
const config = require('../config');

// Validacion de token en todas las solicitudes
app.use(function (req, res, next) {
  var token = req.headers['token'];
  let url = req.originalUrl;
  let method = req.method;

  if (url == '/api/v1/users/' && method == 'POST') {
    next();
  } else {
    crud.Crud(config.connectionMongo).get('users', { token: token }).then(function (result) {
      if (result.length > 0) {
        next();
      } else {
        return res.status(401).send({ success: false, message: 'Invalid access token.' });
      }
    })
  }
});

module.exports = app;