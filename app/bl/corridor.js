

const crud = require('../database/crud');
const config = require('../config');

module.exports = {
  Corridor: function () {
    return new Corridor();
  }
}

class Corridor {
  constructor() {

  }

  async filter(token) {
    try {
      let mongo = await crud.Crud(config.connectionMongo);
      let database = await mongo.open()

      let corridors = await this.getCorridorLikes(token);

      //Consulta de corredores y productos
      var corridors_vector_final = []
      for (let index = 0; index < corridors.length; index++) {
        const element = corridors[index];
        const mongo_corridor = await database.collection('corridors').find({ name: element })
          .project({ _id: 0, name: 1, id: 1 }).toArray();
        var my_corridor = mongo_corridor[0];
        //Lleno el corredor con los productos
        my_corridor.products = await this.fillProducts(database, my_corridor.id)
        corridors_vector_final.push(my_corridor)
      }
      mongo.close();
      return corridors_vector_final;
    } catch (err) {
      return err;
    }
  }

  async getCorridorLikes(token) {
    try {
      let mongo = await crud.Crud(config.connectionMongo);
      let database = await mongo.open()

      let corridors_like_obj = await database.collection('user_likes').find({ token: token, like: true }, { projection: { _id: 0, corridor_name: 1 } }).toArray();
      let corridors_nolike_obj = await database.collection('user_likes').find({ token: token, like: false }, { projection: { _id: 0, corridor_name: 1 } }).toArray();

      var corridornolike_names = []
      corridors_nolike_obj.forEach(element => {
        corridornolike_names.push(element.corridor_name)
      });

      var corridorlikes_names = []
      corridors_like_obj.forEach(element => {
        corridorlikes_names.push(element.corridor_name)
      });

      //Corredores de acuerdo a los gustos
      let corridors = []

      //Corredor que más me gusta
      let corridor_like = this.mode(corridorlikes_names)
      //Corredor que NO me gusta
      let corridor_nolike = this.mode(corridornolike_names)

      //Agrego de primero el corredor que más me gusta
      corridors.push(corridor_like)
      // Agrego los otros corredores que me gustan
      corridorlikes_names.forEach(element => {
        if (!corridors.includes(element)) {
          corridors.push(element)
        }
      });
      //Agrego los corredores que menos me gustan
      corridornolike_names.forEach(element => {
        if (!corridors.includes(element) && element != corridor_nolike) {
          corridors.push(element)
        }
      });
      //Agregar el corredor que no me gusta
      corridors.push(corridor_nolike)

      mongo.close();
      return corridors;
    } catch (err) {
      return err;
    }
  }



  async fillProducts(database, id) {
    var result = await database.collection('products').find(
      { corridors: { $elemMatch: { id: id } } }
    ).project({ _id: 0, name: 1, image: 1, price: 1 }).limit(10).toArray();
    result.forEach(element => {
      element.image = config.urlImage + element.image
    });
    return result;
  }

  mode(array) {
    if (array.length == 0)
      return null;
    var modeMap = {};
    var maxEl = array[0], maxCount = 1;
    for (var i = 0; i < array.length; i++) {
      var el = array[i];
      if (modeMap[el] == null)
        modeMap[el] = 1;
      else
        modeMap[el]++;
      if (modeMap[el] > maxCount) {
        maxEl = el;
        maxCount = modeMap[el];
      }
    }
    return maxEl;
  }

  favcercanos(fav) {
    var correorden = ["Whisky Week", "Licores", "Cigarrillos", "Momento Night Out", "Recarga tu Energia", "Bebidas", "Snacks", "Tu Snack", "Enlatados / sopas y conservas", "Asado", "Pollo / carnes y pescados", "Derivados lácteos", "Congelados", "Refrigerados", "Super Descuentos", "Pastas y granos", "Frutas y verduras", "Despensa", "Panadería", "Preparate para el Desayuno", "Desayuno", "Lunch", "Afterlunch", "Recetas", "Momento De Pelicula", "Insomnio", "Papelería", "Minifarmacia", "Cuidado del hogar", "Cuidado personal", "Bebés y niños", "Zona de belleza", "Disfraces", "Cambio de Clima", "Deporte", "Jueves de Mascotas", "Mascotas"];

    fav = correorden.indexOf(fav);
    if (fav <= 0) {
      var fav2 = fav + 1;
      var fav3 = fav + 2;

    } else if (fav == 36) {
      fav2 = fav - 1;
      fav3 = fav - 2;
    } else {
      fav2 = fav - 1;
      fav3 = fav + 1;
    }
    var fav2s = correorden[fav2];
    var fav3s = correorden[fav3];
    return { f1: fav2s, f2: fav3s }

  }

}



