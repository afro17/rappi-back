const crud = require('../database/crud');
const config = require('../config');
const corridor = require('../bl/corridor');
const prob = require('../bl/prob');

module.exports = {
  Product: function () {
    return new Product();
  }
}

class Product {
  constructor() {
  }

  async filter(token) {
    try {
      console.log('init filter products', token)
      let mongo = await crud.Crud(config.connectionMongo);
      let database = await mongo.open()
      let corridorObj = corridor.Corridor();
      let corridors_name = await corridorObj.getCorridorLikes(token);

      let mylikes = await database.collection('user_likes').count({ token: token });


      if (mylikes == 0) {
        let products = await database.collection('products').aggregate([{ $sample: { size: 10 } }]).toArray();
        mongo.close();
        return products
      }
      else {
        var newProducts = [];

        var ids = await this.getCorredorIds(corridors_name, database)
        var products2 = await database.collection('products').aggregate([{ $sample: { size: 300 } }]).toArray();

        var id1 = ids[0];
        var id2 = ids[1];
        for (let index = 0; index < products2.length; index++) {
          var element = products2[index];
          if (element != undefined && element.corridors[0] != undefined) {
            var myId = element.corridors[0].id

            if (myId == id1 || myId == id2) {
              newProducts.push(element);
            }
          }
          if (newProducts.length == 10) {
            console.log('finish filter products')
            mongo.close();
            return newProducts;
          }
        }
      }


    } catch (err) {
      console.log(err)
      return err;
    }
  }

  async getCorredorIds(corridors, database) {
    try {

      //Consulta de corredores y productos
      var ids = []
      for (let index = 0; index < corridors.length; index++) {
        const element = corridors[index];
        const mongo_corridor = await database.collection('corridors').find({ name: element })
          .project({ _id: 0, name: 1, id: 1 }).toArray();
        var my_corridor = mongo_corridor[0];
        //Lleno los ids
        ids.push(my_corridor.id)
      }
      return ids;
    } catch (err) {
      return err;
    }
  }
}



