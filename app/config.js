var config = {
  //Conection POSTGRES RAPPI
  connectionPostgresRappi: "postgresql://hackathon_med:rappi.the.best.company@postgres-hackathon.eastus2.cloudapp.azure.com:5432/corridors",
  //Conection MONGODB RAPPI
  connectionMongoRappi: "mongodb://hackaton_med:rappi.rulz@mongo-hackathon.eastus2.cloudapp.azure.com:27017/rappi",
  //Conection MONGODB EQUIPO
  connectionMongo: "mongodb://35.175.98.159:27017/rappiDev",
  //URL de las imagenes
  urlImage: "http://img.dev.rappi.com/products/high/"
}
module.exports = config;