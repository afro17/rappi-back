var express = require('express');
const corridors = require('../bl/corridor');
var router = express.Router();

/**
 * Endpoint de corredores 
 */
router.get('/corridors', function (req, res, next) {
  try {
    // crud.Crud(config.connectionMongo).get('corridors', {}).then(function (result) {
    //   res.status(200).send({ success: true, message: 'My corridors', info: result });
    // })
    var token = req.headers['token'];
    corridors.Corridor().filter(token).then(function (result) {
      res.status(200).send({ success: true, message: 'My corridors', info: result });
    })
  } catch (error) {
    res.status(400).send({ success: false, message: 'My corridors', info: error });
  }
});


module.exports = router;