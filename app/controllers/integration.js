var express = require('express');
var crud = require('../database/crud');
var crudPostgres = require('../database/crudPostgres');
var config = require('../config');

var router = express.Router();

/*
Se realizar una integración de datos para migrarlos todos a una nueva base de datos mongoDB
*/

router.get('/rappiProducts', function (req, res, next) {
    try {
        crud.Crud(config.connectionMongoRappi).get('products', {}).then(function (products) {
            crud.Crud(config.connectionMongo).insertMany('products', products).then(function (result) {
                res.status(200).send({ success: true, message: 'Add Rappi Products in new db', info: result });
            })
        })
    } catch (error) {
        res.status(400).send({ success: false, message: 'Rappi Products', info: error });
    }
});

router.get('/rappiCorridors', function (req, res, next) {
    try {
        var sqlQuery = 'select * from corridors'
        crudPostgres.Crud(config.connectionPostgresRappi).get(sqlQuery).then(function (response) {
            var corridors = response.rows;
            crud.Crud(config.connectionMongo).insertMany('corridors', corridors).then(function (result) {
                res.status(200).send({ success: true, message: 'Add Rappi corridors in new db', info: result });
            })
        })
    } catch (error) {
        res.status(400).send({ success: false, message: 'Rappi corridors', info: error });
    }
});

router.get('/rappiSubcorridors', function (req, res, next) {
    try {
        var sqlQuery = 'select * from sub_corridors'
        crudPostgres.Crud(config.connectionPostgresRappi).get(sqlQuery).then(function (response) {
            var sub_corridors = response.rows;
            crud.Crud(config.connectionMongo).insertMany('sub_corridors', sub_corridors).then(function (result) {
                res.status(200).send({ success: true, message: 'Add Rappi sub_corridors in new db', info: result });
            })
        })
    } catch (error) {
        res.status(400).send({ success: false, message: 'Rappi sub_corridors', info: error });
    }
});

router.get('/rappiStores', function (req, res, next) {
    try {
        var sqlQuery = 'select * from stores'
        crudPostgres.Crud(config.connectionPostgresRappi).get(sqlQuery).then(function (response) {
            var stores = response.rows;
            crud.Crud(config.connectionMongo).insertMany('stores', stores).then(function (result) {
                res.status(200).send({ success: true, message: 'Add Rappi stores in new db', info: result });
            })
        })
    } catch (error) {
        res.status(400).send({ success: false, message: 'Rappi stores', info: error });
    }
});



module.exports = router;
