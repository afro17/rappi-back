const express = require('express');
const product = require('../bl/product');
const config = require('../config');

var router = express.Router();

//Endpoint de productos
router.get('/products/', function (req, res, next) {
  try {
    var token = req.headers['token'];
    product.Product().filter(token).then(function (result) {
      result.forEach(element => {
        element.image = config.urlImage + element.image
      });
      res.status(200).send({ success: true, message: 'My Products', info: result });
    })
  } catch (error) {
    res.status(400).send({ success: false, message: 'My Products', info: error });
  }
});


module.exports = router;