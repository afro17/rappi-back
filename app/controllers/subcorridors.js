var express = require('express');
var crud = require('../database/crud');
var config = require('../config');

var router = express.Router();

router.get('/subcorridors', function (req, res, next) {
  try {
    crud.Crud(config.connectionMongo).get('sub_corridors', {}).then(function (result) {
      res.status(200).send({ success: true, message: 'My subcorridors', info: result });
    })
  } catch (error) {
    res.status(400).send({ success: false, message: 'My subcorridors', info: error });
  }
});


module.exports = router;