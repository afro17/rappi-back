var express = require('express');
var crud = require('../database/crud');
var config = require('../config');

var router = express.Router();

router.get('/userlikes/', function (req, res, next) {
  try {
    var token = req.headers['token'];
    crud.Crud(config.connectionMongo).get('user_likes', { token: token }).then(function (result) {
      res.status(200).send({ success: true, message: 'My user like', info: result });
    })
  } catch (error) {
    res.status(400).send({ success: false, message: 'My user like', info: error });
  }
});

router.post('/userlikes', function (req, res, next) {
  try {
    let payload = req.body
    var token = req.headers['token'];
    payload.token = token
    crud.Crud(config.connectionMongo).insert('user_likes', payload).then(function (result) {
      res.status(201).send({ success: true, message: 'OK', info: payload });
    })
  } catch (error) {
    res.status(400).send({ test: true });
  }
});


module.exports = router;