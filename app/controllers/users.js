var express = require('express');
var crud = require('../database/crud');
var config = require('../config');

var router = express.Router();

router.get('/users/', function (req, res, next) {
  try {
    crud.Crud(config.connectionMongo).get('users', {}).then(function (result) {
      res.status(200).send({ success: true, message: 'My users', info: result });
    })
  } catch (error) {
    res.status(400).send({ success: false, message: 'My users', info: error });
  }
});

router.post('/users/', function (req, res, next) {
  try {
    let payload = req.body
    var token = req.headers['token'];

    crud.Crud(config.connectionMongo).get('users', { token: token }).then(function (result) {
      if (result.length == 0) {
        payload.token = token;
        crud.Crud(config.connectionMongo).insert('users', payload).then(function (result) {
          res.status(201).send({ success: true, message: 'OK', info: payload });
        })
      } else {
        res.status(201).send({ success: true, message: 'EXIST', info: payload });
      }
    })
  } catch (error) {
    res.status(400).send({ test: true });
  }
});


module.exports = router;