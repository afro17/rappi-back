'use strict';
const MongoClient = require('mongodb').MongoClient;

module.exports = {
  Crud: function (connection) {
    return new Operation(connection);
  }
}

class Operation {
  constructor(connection) {
    this.url = connection;
    this.db = null;
    this.client = null;
  }

  async insert(collection, payload) {
    try {
      this.client = await MongoClient.connect(this.url);
      this.db = this.client.db();
      let result = await this.db.collection(collection).insertOne(payload);
      this.client.close();
      return result;
    } catch (err) {
      this.client.close();
      return err;
    }
  }
  async insertMany(collection, payload) {
    try {
      this.client = await MongoClient.connect(this.url);
      this.db = this.client.db();
      let result = await this.db.collection(collection).insertMany(payload);
      this.client.close();
      return result;
    } catch (err) {
      this.client.close();
      return err;
    }
  }

  async get(collection, payload) {
    try {
      this.client = await MongoClient.connect(this.url);
      this.db = this.client.db();
      const col = this.db.collection(collection);
      let result = await col.find(payload).toArray();
      this.client.close();
      return result;
    } catch (err) {
      this.client.close();
      return err;
    }
  }

  async getLimit(collection, payload, limit) {
    try {
      this.client = await MongoClient.connect(this.url);
      this.db = this.client.db();
      const col = this.db.collection(collection);
      let result = await col.find(payload).limit(limit).toArray();
      this.client.close();
      return result;
    } catch (err) {
      this.client.close();
      return err;
    }
  }

  async getCollection(collection) {
    try {
      this.client = await MongoClient.connect(this.url);
      this.db = this.client.db();
      return this.db.collection(collection);
    } catch (err) {
      this.client.close();
      return err;
    }
  }

  async open() {
    this.client = await MongoClient.connect(this.url);
    return this.client.db();
  }

  close() {
    this.client.close();
  }

}




