'use strict';

module.exports = {
  Crud: function (connection) {
    return new Operation(connection);
  }
}
class Operation {
  constructor(connection) {
    this.cs = connection;
    const { Pool } = require('pg');
    this.client = new Pool({ connectionString: this.cs });
  }

  async get(sql) {
    return await this.client.query(sql);
  }

}




