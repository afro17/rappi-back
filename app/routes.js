var express = require('express');
var app = express.Router();
const vs = "/api/v1";

// Validacion de autenticación en entorno de produccion

app.use('/', require('./auth/auth'));


//Rutas 
app.use(vs + '/', require('./controllers/integration'));
app.use(vs + '/', require('./controllers/products'));
app.use(vs + '/', require('./controllers/corridors'));
app.use(vs + '/', require('./controllers/subcorridors'));
app.use(vs + '/', require('./controllers/users'));
app.use(vs + '/', require('./controllers/user_likes'));


module.exports = app;