module.exports = {
  apps: [{
    name: 'RAPPI-API',
    script: 'app.js',
    env_development: {
      NODE_ENV: 'development',
      PORT: 3000,
    },
    env_production: {
      NODE_ENV: 'production',
      PORT: 8084,
    }
  }],
};
